<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class User extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users', ['id' => false]);
        $table->addColumn('id', 'biginteger', ['identity' => true]);
        $table->addPrimaryKey('id');
        $table->addColumn('username', 'string', [
            'default' => null,
            'limit' => 256,
            'null' => null,
        ]);
        $table->addColumn('password', 'string', [
            'default' => null,
            'limit' => 256,
            'null' => null,
        ]);
        $table->addColumn('first_name', 'string', [
            'limit' => 50,
            'null' => true,
        ]);
        $table->addColumn('last_name', 'string', [
            'limit' => 50,
            'null' => true,
        ]);
        $table->addColumn('phone', 'string', [
            'limit' => 10,
            'null' => true,
        ]);
        $table->addColumn('address', 'string', [
            'limit' => 256,
            'null' => true,
        ]);
        $table->addColumn('avatar', 'string', [
            'limit' => 256,
            'null' => true,
        ]);
        $table->addColumn('created_by', 'biginteger', [
            'null' => true,
        ]);
        $table->addColumn('created_at', 'datetime', [
            'null' => true,
        ]);
        $table->addColumn('updated_by', 'biginteger', [
            'null' => true,
        ]);
        $table->addColumn('updated_at', 'datetime', [
            'null' => true,
        ]);
        $table->addColumn('deleted_by', 'biginteger', [
            'null' => true,
        ]);
        $table->addColumn('deleted_at', 'datetime', [
            'null' => true,
        ]);

        $table->create();
    }
}

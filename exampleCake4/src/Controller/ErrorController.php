<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Error Controller
 *
 *
 * @method \App\Model\Entity\Error[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ErrorController extends AppController
{
    /**
     *
     *
     * @return \Cake\Http\Response|null
     */
}

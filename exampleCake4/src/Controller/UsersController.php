<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * Login
     *
     * @return \Cake\Http\Response|null
     */
    public function login() {
        $this->set('title', 'Login');
        if($this->request->is('post')) {
            $params = $this->request->getData();
            //$this->loadModel('Users');
            // function login check email, password
            $userLogin = $this->Users->login($params);
            // email or password fail
            if(empty($userLogin)) {
                return $this->Flash->error('login fail!');
            }
            // Duplicated user case
            if(count($userLogin) > 1) {
                return $this->Flash->error('login fail!');
            }
            return $this->redirect('/list');
        }
    }


    /**
     * List User Info
     *
     * @return \Cake\Http\Response|null
     */
    public function listUser() {
        // get list user info
        $usersInfo = $this->Users->getListUserInfo();
        //dd($usersInfo);
        $this->set('data', $usersInfo);
    }

    /**
     * User Register
     *
     * @return \Cake\Http\Response|null
     */
    public function register() {
        $this->set('title', 'Register');
        if($this->request->is('post')) {
            $data = $this->Users->newEntity($this->request->getData());
            //dd($data);
            if($this->Users->save($data)) {
                return $this->Flash->success('Register success!');
            } else {
                return $this->Flash->error('Register fail!');
            }
        }
    }

    /**
     * User Edit
     *
     * @return \Cake\Http\Response|null
     */
    public function edit($id = null) {
        $this->set('id', $id);
    }
}

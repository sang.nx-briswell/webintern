<html>
    <header>
        <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery.min') ?>"></script>
        <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery.validate') ?>"></script>
        <script type="text/javascript" src="<?= $this->Url->script('additional-methods') ?>"></script>
        <script type="text/javascript" src="<?= $this->Url->script('web/user/user') ?>"></script>

    </header>
    <body>
        <div>
            <h2>
                <?php echo $title?>
            </h2>
            <span class="text-danger">
                <?= $this->Flash->render() ?>
            </span>
            <?php
            echo $this->Form->create(null, [
                'url' => '/login',
                'type' => 'post',
                'id' => 'formLogin',
                //'novalidate' => true
            ]);
            echo $this->Form->control('email', [
                'label' => [
                    'text' => 'Email:'
                ],
                'class' => 'form-control',
                'type' =>   'email',
                'placeholder' => 'Enter your email',
                 array('class required email')
            ]);
            echo $this->Form->control('password', [
                'label' => [
                    'text' => 'Password:'
                ],
                'class' => 'form-control',
                'type' => 'password',
                'placeholder' => 'Enter your password',
                array('class required')
            ]);
            echo $this->Form->submit($title, [
                'class' => 'btn btn-info'
            ]);
            ?>
        </div>
    </body>
</html>

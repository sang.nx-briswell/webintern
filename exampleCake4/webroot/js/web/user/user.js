$(document).ready(function () {
    $('#formLogin').validate({
        rules: {
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true,
            }
        },
        messages: {
            email: {
                required: "please enter your email",
                email: "email not correct",
            },
            password: "please enter your password",
        }
    });
});
